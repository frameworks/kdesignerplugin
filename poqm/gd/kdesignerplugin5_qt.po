# GunChleoc <fios@foramnagaidhlig.net>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2015-11-04 15:12+0000\n"
"Last-Translator: Michael Bauer <fios@akerbeltz.org>\n"
"Language-Team: Fòram na Gàidhlig\n"
"Language: gd\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : "
"(n > 2 && n < 20) ? 2 : 3;\n"
"X-Generator: Poedit 1.8.4\n"
"X-Qt-Contexts: true\n"

#: kgendesignerplugin.cpp:88
msgctxt "main|"
msgid "Builds Qt widget plugins from an ini style description file."
msgstr ""
"Togaidh seo plugain Qt widget o fhaidhle mìneachaidhean stoidhlichean ini."

#: kgendesignerplugin.cpp:95
msgctxt "main|"
msgid "Input file."
msgstr "Faidhle an ion-chuir."

#: kgendesignerplugin.cpp:96
msgctxt "main|"
msgid "Output file."
msgstr "Faidhle an às-chuir."

#: kgendesignerplugin.cpp:97
msgctxt "main|"
msgid ""
"Name of the plugin class to generate (deprecated, use PluginName in the "
"input file)."
msgstr ""
"An t-ainm air a' chlas plugain ri ghintinn (cha mholar seo tuilleadh, "
"cleachd PluginName ann am faidhle an ion-chuir 'na àite)."

#: kgendesignerplugin.cpp:98
msgctxt "main|"
msgid ""
"Default widget group name to display in designer (deprecated, use "
"DefaultGroup in the input file)."
msgstr ""
"Ainm tùsail air buidheann widget a nochdas sa cho-dhealbhaiche (cha mholar "
"seo tuilleadh, cleachd DefaultGroup ann am faidhle an ion-chuir 'na àite)."

#: kgendesignerplugin.cpp:101
msgctxt "kgendesignerplugin about data|"
msgid "kgendesignerplugin"
msgstr "Plugan gintinn co-dhealbhaiche"

#: kgendesignerplugin.cpp:105
msgctxt "kgendesignerplugin about data|"
msgid "(C) 2004-2005 Ian Reinhart Geiser"
msgstr "(C) 2004-2005 Ian Reinhart Geiser"

#: kgendesignerplugin.cpp:109
msgctxt "kgendesignerplugin about data|"
msgid "Ian Reinhart Geiser"
msgstr "Ian Reinhart Geiser"

#: kgendesignerplugin.cpp:110
msgctxt "kgendesignerplugin about data|"
msgid "Daniel Molkentin"
msgstr "Daniel Molkentin"
